package tk.baumi.jaraco;

public class IPEncryption {

	public IPEncryption() {
	}
	
	public byte[] encrypt(byte[] input, char[] key) {
		byte[] bKey = new byte[key.length];
		for(int i=0; i<key.length; i++) {
			bKey[i] = (byte) key[i];
		}
		return encrypt(input, bKey);
	}
	
	public byte[] encrypt(byte[] input, byte[] key) {
		byte[] output = input;
		int i = 0;
		int over4 = output.length % 4;
		for(; i < output.length - over4; i+=4) {
			byte[] help = new byte[] {output[i], output[i + 1], output[i + 2], output[i + 3]};
			output[i] = leftShift((byte) (help[2] ^ key[0]), key[0]);
			output[i + 1] = rightShift((byte) (help[3] ^ key[1]), key[1]);
			output[i + 2] = rightShift((byte) (help[1] ^ key[2]), key[2]);
			output[i + 3] = leftShift((byte) (help[0] ^ key[3]), key[3]);
		}
		for(; i < output.length; i++) {
			output[i] = (byte) (output[i] ^ key[i % 4]);
		}
		return output;
	}
	
	public byte[] decrypt(byte[] input, byte[] key) {
		byte[] output = input;
		int over4 = output.length % 4;
		int i = 0;
		for(; i < output.length - over4; i+=4) {
			byte[] help = new byte[]{output[i], output[i + 1], output[i + 2], output[i + 3]};
			output[i + 2] = (byte) (rightShift(help[0], key[0]) ^ key[0]);
			output[i + 3] = (byte) (leftShift(help[1], key[1]) ^ key[1]);
			output[i + 1] = (byte) (leftShift(help[2], key[2]) ^ key[2]);
			output[i + 0] = (byte) (rightShift(help[3], key[3]) ^ key[3]);
		}
		for(;i < output.length; i++) {
			output[i] = (byte) (output[i] ^ key[i % 4]);
		}
		return output;
	}
	
	public byte[] decrypt(byte[] input, char[] key) {
		byte[] bKey = new byte[key.length];
		for(int i=0; i<key.length; i++) {
			bKey[i] = (byte) key[i];
		}
		return decrypt(input, bKey);
	}
	
	public byte rightShift(byte b) {
		byte ret = 0;
		//0110 1111
		//1011 0111
		if(b % 2 == 0) {
			if(b < 0) {
				ret = (byte) (b >> 1);
				ret = (byte) (ret ^ 0x80);
			} else {
				ret = (byte) (b >> 1);
			}
		} else {
			ret = (byte) (b >> 1);
			ret = (byte) (ret | 0x80);
		}
		return ret;
	}
	
	public byte rightShift(byte b, byte amount) {
		for(int i=0;i<amount;i++) {
			b = rightShift(b);
		}
		return b;
	}
	
	public byte leftShift(byte b) {
		byte ret = 0;
		if(b < 0) {
			ret = (byte) (b << 1);
			ret += 1;
		} else {
			ret = (byte) (b << 1);
		}
		return ret;
	}
	
	public byte leftShift(byte b, byte amount) {
		for(int i=0;i<amount;i++) {
			b = leftShift(b);
		}
		return b;
	}
	
	public static byte[] intToByteArray(int i) {
		byte[] b = new byte[4];
		b[0] = (byte) (i % 256);
		b[1] = (byte) ((i >> 8) % 256);
		b[2] = (byte) ((i >> 16) % 256);
		b[3] = (byte) ((i >> 24) % 256);
		return b;
	}
	
	public static int byteArrayToInt(byte[] b) 
	{
	    return   b[0] & 0xFF |
	    		(b[1] & 0xFF) << 8 |
	            (b[2] & 0xFF) << 16 |
	            (b[3] & 0xFF) << 24;
	}
	
	public static int byteToInt(byte[] b) {
		int r1 = b[0];
		if(r1 < 0) {
			r1 = 256 - Math.abs(r1);
		}
		int r2 = b[1];
		if(r2 < 0) {
			r2 = Math.abs(b[1] << 8);
		} else {
			r2 = r2 << 8;
		}
		//int r4 = b[3] << 24;
		return r1 + r2;
	}
}
