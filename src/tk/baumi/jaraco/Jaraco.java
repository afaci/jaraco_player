package tk.baumi.jaraco;

import org.farng.mp3.MP3File;

/**
 * The class Jaraco is the main-class of the project.<br/>
 * It starts all components and also provides constants for the Jaraco-protocol
 * over the network.
 * @author Manuel Baumgartner
 *
 */

public class Jaraco {
	/**
	 * MTU is essential for transmitting data over the network<br/>
	 * After some research a MTU of 1024 bytes was the best.
	 */
	public static short MTU = 1024;
	/**
	 * All other constants are uses by to protocol to split packets into
	 * particular operations.<br/>
	 * Not all operations are used by the moment.
	 */
	public static byte TYPE_POSITION = 1;
    public static byte TYPE_SONGNAME = 2;
    public static byte TYPE_PLAYLIST = 3;
    public static byte TYPE_PLAYLIST_ADD = 4;
    public static byte TYPE_ULSONG = 6;
    public static byte TYPE_ULSONG_ADD = 7;
    public static byte TYPE_FOLDER = 8;
    public static byte TYPE_FOLDER_ADD = 9;
    public static byte TYPE_DLSONG = 10;
    public static byte TYPE_DLSONG_ADD = 11;
    public static byte TYPE_VOLUME = 12;
    public static byte TYPE_ADDSONG = 13;
    public static byte TYPE_PHONE_STOP = 14;
    public static byte TYPE_ULSONG_END = 15;
    public static byte TYPE_VOTE = 16;
    
    public static short DEFAULT_PORT = 8800;
	
	public Jaraco() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * Not all ID-tags are in UTF8, therefore they was to be converted.
	 * @param mtitle the UTF8-representation of the given String.
	 * @return
	 */
	public static String toUTF8(String mtitle) {
		String title = "";
		if(mtitle != null) {
			for(int i = 0; i < mtitle.length(); i++) {
				if(mtitle.charAt(i) != 0 && mtitle.charAt(i) != 65533) {
					title += mtitle.charAt(i);
				}
			}
		}
		return title;
	}
	/**
	 * Gets the MP3-ID-tags for a specified file.
	 * This method is static, because it is used by many classes.
	 * @param file The file as string. (full-path).
	 * @return An Array of the following elements:
	 * 			<ul>
	 * 			<li>0 The Artist (Lead-artist)</li>
	 * 			<li>1 The Album (Album-title)</li>
	 * 			<li>2 The title (Song-title)</li>
	 * 			</ul>
	 */
	public static String[] getIDTags(String file) {
		String[] ret = new String[3];
		try {
			MP3File mp3 = new MP3File(file);
			String mtitle = null, martist=null, malbum=null;
			if(mp3.hasID3v1Tag()) {
				mtitle = mp3.getID3v1Tag().getTitle();
				martist = mp3.getID3v1Tag().getArtist();
				malbum = mp3.getID3v1Tag().getAlbum();
			} else if(mp3.hasID3v2Tag()) {
				mtitle = mp3.getID3v2Tag().getSongTitle();
				martist = mp3.getID3v2Tag().getLeadArtist();
				malbum = mp3.getID3v2Tag().getAlbumTitle();
			}
			ret[0] = toUTF8(martist);
			ret[1] = toUTF8(malbum); 
			ret[2] = toUTF8(mtitle);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}
	
	
	/**
	 * <p>The main-class firstly starts a new player-instance,<br/>
	 * Then it starts the GUI with the player as parameter.</p>
	 * <p>When the player has been initialized the GUI will be shown</p>
	 * <p>Last but not least the most important function will start<br/>
	 * The network-listener for providing remote-control.</p>
	 * @param args
	 */
	public static void main(String[] args) {
		JaracoPlayer p = new JaracoPlayer();
		//files.add(folder + "011. Martin Solveig & The Cataracs feat. Kyle - Hey Now - www.musicasparabaixar.org.mp3");
		//files.add(folder + "029. Massari - Habibi (Brand New Day) - www.musicasparabaixar.org.mp3");
		JaracoGUI gui = new JaracoGUI(p);
		gui.setVisible(true);
		int port = DEFAULT_PORT;
		if(args.length > 0) {
			try {
			port = Integer.parseInt(args[0]);
			} catch (NumberFormatException e) {
				System.err.println("Port is not a valid number");
			}
		}
		
		NetworkController nController = new NetworkController(port, p, gui);
		nController.getClass();
	}
	

}
