package tk.baumi.jaraco;

import java.io.File;
import java.net.InetAddress;
import java.util.ArrayList;
/**
 * <p>The class MusicFile is for saving and comparing informations about each song.<br/>
 * It also adds, controls and count the current votes.<br/></p>
 * <p>With the interface Comparable it is possible to order the list automatically
 * by the number of votes. All elements are added by date and stay in this order
 * without any changes</p>
 * 
 * @author Manuel Baumgartner
 *
 */
public class MusicFile implements Comparable<MusicFile>{
	private String filename;
	private String name;
	public ArrayList<InetAddress> votes;
	
	/**
	 * The standard-constructor just initializes the voting-system for this song.
	 */
	public MusicFile() {
		votes = new ArrayList<InetAddress>();
	}
	/**
	 * When an user votes the system checks if he or she has already voted.<br/>
	 * It adds the vote only if the user hasn't had voted.
	 * @param vote
	 * @return
	 */
	public boolean addVote(InetAddress vote) {
		if(!votes.contains(vote)) {
			votes.add(vote);
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * This constructor creates all information to this file from it's filename.
	 * @param filename The filename that provides all ID-Tags.
	 */
	public MusicFile(String filename) {
		this.filename = filename;
		String[] idTags = Jaraco.getIDTags(filename);
		int slash = filename.lastIndexOf(File.separator);
		if(idTags[2].length() == 0 && idTags[0].length() == 0) {
			this.name = filename.substring(slash + 1, filename.length());
		} else {
			this.name = idTags[0] + " - " + idTags[2];
		}
		votes = new ArrayList<InetAddress>();
	}
	
	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
		int slash = filename.lastIndexOf(File.pathSeparator);
		this.name = filename.substring(slash, filename.length());
	}

	public String getName() {
		return name;
	}
	
	public String toString() {
		if(name != null) {
			return name + " ("+getVotes()+")";
		} else {
			return super.toString();
		}
	}
	
	public int getVotes() {
		return votes.size();
	}
	
	public String getExtension() {
		int point = filename.lastIndexOf(".");
		return filename.substring(point + 1).toLowerCase();
	}

	@Override
	public int compareTo(MusicFile arg0) {
		return this.votes.size() - arg0.votes.size();
	}
}
