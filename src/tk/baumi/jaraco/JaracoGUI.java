package tk.baumi.jaraco;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.DropMode;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.MatteBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileFilter;

import tk.baumi.jaraco.JaracoPlayer.OnPlayingListener;
import tk.baumi.jaraco.JaracoPlayer.PlayingChangeListener;
import tk.baumi.jaraco.NetworkController.CloseSocket;
import javazoom.jl.decoder.JavaLayerException;

/**
	 * <p>These JaracoGUI class is the graphical user interface for the
	 * desktop user.<br/>
	 * It makes it possible to control the whole progress of
	 * <ul>
	 * <li>adding</li>
	 * <li>voting</li>
	 * <li>removing</li>
	 * </ul>
	 * files.</p>
 * @author Manuel Baumgartner, Julian Gaisbauer
 *
 */
public class JaracoGUI extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = -8678674304222185534L;
	private JButton btPlay, btStop, btNext, btBefore, btAdd, btRemove, btUp, btDown, btLike;
	private JToggleButton tbRepeat;
	private JaracoPlayer playerInstance;
	private DefaultListModel<MusicFile> listModel;
	private DefaultListModel<String> lmUser;
	private JList<MusicFile> liPlaylist;
	private JList<String> liUser;
	private JSlider slPosition, slVolume;
	private JMenuBar menuMain;
	private JMenu filesMenu, networkMenu;
	private JMenuItem miFileAdd;
	private JCheckBoxMenuItem miPhoneStop;
	private int newPos = -1;
	private PlayingChangeListener playChange;
	private OnPlayingListener playingListener;
	private CloseSocket socketClose = null;
	private byte changePos = 0;
	private JLabel lTitle, lInterpret, lAlbum;
	private BufferedImage biButtons, pressedButtons;
	public static String userMusic = System.getProperty("user.home") + File.separator + "Music";
	private static Color bgColor = new Color(8,1,34);
	private ImageView imAmp;
	private BufferedImage[] biAmp;
	private boolean phoneStop = true;
	
	/**
	 * The constructor has to take a not-null playerInstance to make controls on
	 * the player via button- and other mouse-events.
	 * @param playerInstance The playerInstance that mustn't be null.
	 */
	public JaracoGUI(JaracoPlayer playerInstance) {
		this.playerInstance = playerInstance;
		onCreate();
	}
	
	/**
	 * The onCreate-Method is called by the JaracoGUI(JaracoPlayer)-Constructor and builds the GUI for the Player
	 * with all Buttons, Sliders, PlayList, AmplitudeView and NetworkConnections
	 * @author Manuel Baumgartner and Julian Gaisbauer
	 */
	public void onCreate() {
		setTitle("JaracoPlayer");
		Image img = null;
		try {
			/**
			 * The ToolKit loads the image from the launcher-icon from the image folder.
			 * The icon of the program is set with this method.
			 */
			img = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/ic_launcher.png"));
			setIconImage(img);
		} catch(Exception e) {
			System.err.println("Error: " + e.getMessage());
		}
		setBounds(0, 10, 1200, 600);
		
		setLayout(new BorderLayout());
		
		//North-Layout for the Menu
		menuMain = new JMenuBar();
		
		filesMenu = new JMenu("Files");
		miFileAdd = new JMenuItem("Add File");
		miFileAdd.addActionListener(this);
		filesMenu.add(miFileAdd);
		
		
		networkMenu = new JMenu("Network");
		
		miPhoneStop = new JCheckBoxMenuItem("Call stop");
		miPhoneStop.setState(true);
		/**
		 * These method occurs, when the checkbox in the menu has been changed.
		 */
		miPhoneStop.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				phoneStop = miPhoneStop.getState();
			}
		});
		
		networkMenu.add(miPhoneStop);
		menuMain.add(filesMenu);
		menuMain.add(networkMenu);
		
		//South-Layout for the Player-Buttons
		JPanel jpPlayerButtons = new JPanel(new FlowLayout());
		
		
		btPlay = new JButton();
		btPlay.setBackground(bgColor);	
		
		btStop = new JButton();
		btStop.setBackground(bgColor);
		
		btNext = new JButton();
		btNext.setBackground(bgColor);
		
		btBefore = new JButton();
		btBefore.setBackground(bgColor);
		
		tbRepeat = new JToggleButton();
		tbRepeat.setBackground(bgColor);
		
		slPosition = new JSlider();
		slVolume = new JSlider();
		
		try {
			biButtons = ImageIO.read(getClass().getResource("/images/jaraco_buttons.png"));
			pressedButtons = ImageIO.read(getClass().getResource("/images/jaraco_buttons_pressed.png"));
		} catch (IOException e2) {
			System.err.println("Error image of buttons wasn't found: " + e2.getMessage());
		}
		//Pictures and Border for the Player-Buttons
		try {
			//Replaces the margin of the buttons.
			Border p = new MatteBorder(5, 15, 5, 15, bgColor);
			
			//The getSubimage function returns a specified area of an image.
			img = biButtons.getSubimage(50, 50, 50, 50);
			btPlay.setIcon(new ImageIcon(img));
			img = pressedButtons.getSubimage(50, 50, 50, 50);
			btPlay.setPressedIcon(new ImageIcon(img));
			btPlay.setBorder(p);			
			
			img = biButtons.getSubimage(150, 50, 50, 50);
			btStop.setIcon(new ImageIcon(img));
			img = pressedButtons.getSubimage(150, 50, 50, 50);
			btStop.setPressedIcon(new ImageIcon(img));
			btStop.setBorder(p);
			
			img = biButtons.getSubimage(50, 0, 50, 50);
			btNext.setIcon(new ImageIcon(img));
			img = pressedButtons.getSubimage(50, 0, 50, 50);
			btNext.setPressedIcon(new ImageIcon(img));
			btNext.setBorder(p);
			
			img = biButtons.getSubimage(0, 0, 50, 50);
			btBefore.setIcon(new ImageIcon(img));
			img = pressedButtons.getSubimage(0, 0, 50, 50);
			btBefore.setPressedIcon(new ImageIcon(img));
			btBefore.setBorder(p);
			
			img = biButtons.getSubimage(100, 0, 50, 50);
			tbRepeat.setIcon(new ImageIcon(img));
			img = pressedButtons.getSubimage(100, 0, 50, 50);
			tbRepeat.setPressedIcon(new ImageIcon(img));
			tbRepeat.setBorder(p);
			
		} catch (Exception e1) {
			System.err.println("Error loading images: " + e1.getMessage());
		}
		//The UIManager can change all standard-implementations without overriding
		UIManager.put("ToggleButton.select", Color.RED);
		SwingUtilities.updateComponentTreeUI(tbRepeat);
		
		//Action-Listeners for the Player-Buttons
		btPlay.addActionListener(this);//		
		btStop.addActionListener(this);
		btNext.addActionListener(this);
		btBefore.addActionListener(this);
		
		slPosition.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				changePos = 1;
				newPos = -1;
			}
			@Override
			public void mouseReleased(MouseEvent e) {
				changePos = 0;
				try {
					playerInstance.setPosition(newPos);
				} catch (Exception ex) {}
			}
		});
		
		slPosition.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				if(changePos != 0) {
					newPos = slPosition.getValue();
				}
			}
		});
		
		slVolume.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent arg0) {
				float val = (float)slVolume.getValue() / slVolume.getMaximum() * 2;
				//System.out.println(val);
				playerInstance.setVolume(val);
			}
		});
		
		tbRepeat.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent arg0) {
				if(arg0.getStateChange() == ItemEvent.SELECTED) {
					playerInstance.setRepeat(true);
					tbRepeat.setIcon(new ImageIcon(biButtons.getSubimage(150, 0, 50, 50)));
				} else if(arg0.getStateChange() == ItemEvent.DESELECTED) {
					playerInstance.setRepeat(false);
					tbRepeat.setBackground(null);
					tbRepeat.setIcon(new ImageIcon(biButtons.getSubimage(100, 0, 50, 50)));
				}
			}
		});

		
		//Center-Layout for the playlist
		JPanel jpPlaylist = new JPanel(new BorderLayout());
		listModel = playerInstance.getList();
		liPlaylist = new JList<MusicFile>(listModel);
	
		liPlaylist.setDragEnabled(true);
		liPlaylist.setDropTarget(new DropTarget() {
	        /**
			  * These methods makes it possible to add files per drag-and-drop
			  * from the file-manager.
			  * 
			  * It also checks if the given file is an .mp3 or .m3u file.
			 */
			private static final long serialVersionUID = 1766231036261266981L;
			public synchronized void drop(DropTargetDropEvent evt) {
	            try {
	                evt.acceptDrop(DnDConstants.ACTION_COPY);
	                @SuppressWarnings("unchecked")
					List<File> droppedFiles = (List<File>) evt
	                        .getTransferable().getTransferData(
	                                DataFlavor.javaFileListFlavor);
	                for (File file : droppedFiles) {
                		addMP3Files(file);
	                }
	            } catch (Exception ex) {
	                ex.printStackTrace();
	            }
	        }
	    });
		liPlaylist.setDropMode(DropMode.INSERT);
		//TransferHandler tf = new TransferHandler(getName());
		//Center-south layout for the playlist-Buttons
		JPanel jpPlaylistButtons = new JPanel(new FlowLayout());
		btAdd = new JButton();
		btRemove = new JButton();
		btUp = new JButton();
		btDown = new JButton();
		btLike = new JButton();
		//Image image = Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/jaraco_buttons.png"));
		
		
		//Images for the playlist-Buttons
		try {
			//Border p = new LineBorder(bgColor);			
			//Insets margin = new Insets(0,50,0,50);
			Border p = new MatteBorder(20, 15, 5, 15, bgColor);
			
			btAdd.setIcon(new ImageIcon(biButtons.getSubimage(0, 100, 50, 50)));
			btAdd.setPressedIcon(new ImageIcon(pressedButtons.getSubimage(0, 100, 50, 50)));
			btAdd.setBackground(bgColor);
			btAdd.setBorder(p);
			//btAdd.setBorderPainted(false);
			
			
			btRemove.setIcon(new ImageIcon(biButtons.getSubimage(50, 100, 50, 50)));
			btRemove.setPressedIcon(new ImageIcon(pressedButtons.getSubimage(50,100,50,50)));			
			btRemove.setBackground(bgColor);
			btRemove.setBorder(p);
			//btRemove.setBorderPainted(false);
		
			
			btUp.setIcon(new ImageIcon(biButtons.getSubimage(100, 100, 50, 50)));
			btUp.setPressedIcon(new ImageIcon(pressedButtons.getSubimage(100, 100, 50, 50)));
			btUp.setBackground(bgColor);
			btUp.setBorder(p);
			//btUp.setMargin(margin);
			//btUp.setBorderPainted(false);
			
			btDown.setIcon(new ImageIcon(biButtons.getSubimage(150, 100, 50, 50)));
			btDown.setPressedIcon(new ImageIcon(pressedButtons.getSubimage(150, 100, 50, 50)));
			btDown.setBackground(bgColor);
			btDown.setBorder(p);
			//btDown.setBorderPainted(false);
			
			btLike.setIcon(new ImageIcon(biButtons.getSubimage(0, 50, 50, 50)));
			btLike.setPressedIcon(new ImageIcon(pressedButtons.getSubimage(0, 50, 50, 50)));
			btLike.setBackground(bgColor);
			btLike.setBorder(p);
			
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		//ActionsEvents for the playlist-Buttons
		btAdd.addActionListener(this);
		btRemove.addActionListener(this);
		btUp.addActionListener(this);
		btDown.addActionListener(this);
		btLike.addActionListener(this);
		
		//The mouse listenes for playing the double-clicked file.
		liPlaylist.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent arg0) {
				if(arg0.getClickCount() == 2 && arg0.getButton() == MouseEvent.BUTTON1) {
					try {
						playerInstance.resetPosition();
						playerInstance.playFile(
								liPlaylist.getSelectedIndex());
					} catch (IOException e) {
						e.printStackTrace();
					} catch (JavaLayerException e) {
						e.printStackTrace();
					}
				} 
			}
			
		});
		//User-List
		lmUser = new DefaultListModel<String>();		
		liUser = new JList<String>(lmUser);
		liUser.setBackground(bgColor);
		liUser.setForeground(Color.white);		
		
		
		this.add(liUser, BorderLayout.EAST);
		
		//Current-playing
		
		JPanel jpCurrent = new JPanel(new BorderLayout());
		biAmp = new BufferedImage[7];
		try {
			biAmp[0] = ImageIO.read(getClass().getResource("/images/amp_0.png"));
			biAmp[1] = ImageIO.read(getClass().getResource("/images/amp_1.png"));
			biAmp[2] = ImageIO.read(getClass().getResource("/images/amp_2.png"));
			biAmp[2] = ImageIO.read(getClass().getResource("/images/amp_3.png"));
			biAmp[4] = ImageIO.read(getClass().getResource("/images/amp_4.png"));
			biAmp[5] = ImageIO.read(getClass().getResource("/images/amp_5.png"));
			biAmp[6] = ImageIO.read(getClass().getResource("/images/amp_6.png"));
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		/*
		 * The image-view is for showing the current amplitude.
		 */
		imAmp = null;
		try {
			imAmp = new ImageView();
			imAmp.setImage(biAmp[0]);
			imAmp.setSize();
			imAmp.setBackground(bgColor);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		playerInstance.setAmplitudeChangeListener(new JaracoPlayer.OnAmplitudeChangeListener() {
			
			@Override
			public void amplitudeChange(byte value) {
				setAmplitude(value);
			}
		});
		
		JPanel jpTags = new JPanel(new BorderLayout());
		
		lInterpret = new JLabel();
		lInterpret.setFont(new Font("Arial", Font.BOLD, 30));
		lInterpret.setForeground(Color.white);
		
		lTitle = new JLabel();
		lTitle.setFont(new Font("Arial", Font.PLAIN, 20));
		lTitle.setForeground(Color.white);
		
		lAlbum = new JLabel();
		lAlbum.setFont(new Font("Arial", Font.ITALIC, 20));
		lAlbum.setForeground(Color.white);
		
		jpTags.add(lInterpret, BorderLayout.NORTH); 
		jpTags.add(lTitle, BorderLayout.CENTER); 
		jpTags.add(lAlbum, BorderLayout.SOUTH);
		jpTags.setBackground(bgColor);
		jpTags.setForeground(Color.white);
		
		jpCurrent.add(jpTags, BorderLayout.NORTH);
		jpCurrent.add(imAmp, BorderLayout.CENTER);
		this.add(jpCurrent, BorderLayout.WEST);
		//Playerbuttons - Add
		jpPlayerButtons.add(btPlay);
		jpPlayerButtons.add(btStop);
		jpPlayerButtons.add(btBefore);
		jpPlayerButtons.add(btNext);
		jpPlayerButtons.add(tbRepeat);
		jpPlayerButtons.setBackground(bgColor);
		JPanel southPanel = new JPanel(new GridLayout(3, 2));
		southPanel.add(slPosition, BorderLayout.SOUTH);
		slPosition.setBackground(bgColor);
		slVolume.setBackground(bgColor);
		southPanel.add(slVolume, BorderLayout.WEST);
		southPanel.add(jpPlayerButtons, BorderLayout.SOUTH);
		this.add(southPanel, BorderLayout.SOUTH);
		
		//Playlistbuttons - Add
		jpPlaylistButtons.add(btAdd);
		jpPlaylistButtons.add(btRemove);
		jpPlaylistButtons.add(btUp);
		jpPlaylistButtons.add(btDown);
		jpPlaylistButtons.add(btLike);
		jpPlaylistButtons.setBackground(bgColor);
		
		Color liColor = new Color(20,25,65);
		
		JScrollPane jpScroll = new JScrollPane(liPlaylist);
		jpScroll.setBackground(liColor);
		
		jpScroll.setBorder(new MatteBorder(5, 5, 5, 5, liColor));	
		
		jpPlaylist.add(jpScroll, BorderLayout.CENTER);		
		//jpPlaylist.setBackground(new Color(20,25,65));
		liPlaylist.setBackground(new Color(20,25,65));
		//liPlaylist.setBorder(new MatteBorder(5, 5, 5, 5, new Color(20,25,65)));
		//jpPlaylist.setBorder(new MatteBorder(5, 5, 5, 5, new Color(20,25,65)));
		
		liPlaylist.setForeground(Color.white);
		jpPlaylist.add(jpPlaylistButtons, BorderLayout.SOUTH);
		this.add(jpPlaylist);
		this.add(menuMain, BorderLayout.NORTH);
		//Listener for starting playback and marking the current file in playlist.
		playChange = new PlayingChangeListener() {
			
			@Override
			public void changePlay(int currFile, String filename, boolean playing) {
				setCurrent(currFile);
				try {
					String[] idtags = Jaraco.getIDTags(filename);
					setIDTags(idtags[0], idtags[1], idtags[2]);
				} catch (Exception e) {
					e.printStackTrace();
				} 
				
				if(playing) {
					//btPlay.setText("Pause");
					try {
						btPlay.setIcon(new ImageIcon(biButtons.getSubimage(100, 50, 50,50)));
						btPlay.setPressedIcon(new ImageIcon(pressedButtons.getSubimage(100, 50, 50, 50)));
					} catch (Exception ex) {
						
					}
				} else {
					//btPlay.setText("Play");
					setAmplitude((byte)0);
					try {
						btPlay.setIcon(new ImageIcon(biButtons.getSubimage(50, 50, 50,50)));
						btPlay.setPressedIcon(new ImageIcon(pressedButtons.getSubimage(50, 50, 50, 50)));
					} catch (Exception ex) {
						
					}
				}
			}
		};
		//Event for setting the current position
		playingListener = new JaracoPlayer.OnPlayingListener() {
			
			@Override
			public void onPlaying(int position, int max) {
				if(changePos == 0) {
					slPosition.setMaximum(max);
					slPosition.setValue(position);
				}
			}
		};
		
		
		
		playerInstance.setPlayingChangeListener(playChange);
		playerInstance.setOnPlayingListener(playingListener);
		//Closing-Event for the GUI
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				if(playerInstance.isInitialized()) {
					playerInstance.stop();
				}
				if(socketClose != null) {
					socketClose.closeSocket();
				}
				setVisible(false);
				dispose();
			}
		});
		setVisible(true);
	} // end on create

	public void setCloseSocket(CloseSocket socketClose) {
		this.socketClose = socketClose;
	}
	
	public boolean getPhoneStop() {
		return phoneStop;
	}

	
	//ActionEvents seperated by the button-ID
	@Override
	public void actionPerformed(ActionEvent e) {		
		if(e.getSource() == btPlay) {
			try {
				playerInstance.play();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (JavaLayerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if(e.getSource() == btStop) {
			playerInstance.stop();
		} else if(e.getSource() == btNext) {
			try {
				playerInstance.playNext();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (JavaLayerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if(e.getSource() == btBefore) {
			try {
				playerInstance.playBefore();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (JavaLayerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		} else if(e.getSource() == miFileAdd || e.getSource() == btAdd) {
			/*
			 * Starts a File-dialog to add a mp3 or m3u file.
			 */
			JFileChooser fileChoose = new JFileChooser(new File(userMusic));
			fileChoose.setMultiSelectionEnabled(true);
			fileChoose.setFileFilter(new FileFilter() {
				
				@Override
				public String getDescription() {
					return null;
				}
				
				@Override
				public boolean accept(File arg0) {
					String filename = arg0.getName();
					if(filename.endsWith("mp3") ||
							filename.endsWith("MP3") ||
							filename.endsWith("mP3") ||
							filename.endsWith("Mp3") ||
							filename.endsWith("m3u") ||
							arg0.isDirectory()) {
						return true;
					} else {
						return false;
					}
				}
			});
			if(fileChoose.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
				File[] files = fileChoose.getSelectedFiles();
				for(int i=0; i<files.length; i++) {
					File f = files[i];
					addMP3Files(f);
				}
			}
		} else if(e.getSource() == btRemove) {
			int select = liPlaylist.getSelectedIndex();
			if(select >= 0) {
				playerInstance.removeFile(select);
			}
		} else if(e.getSource() == btUp) {
			int select = liPlaylist.getSelectedIndex();
			if(select > 0) {
				playerInstance.changePosition(select - 1, select);
				liPlaylist.setSelectedIndex(select - 1);
			}
		} else if(e.getSource() == btDown) {
			int select = liPlaylist.getSelectedIndex();
			if(select >= 0 && select < listModel.size() - 1) {
				playerInstance.changePosition(select + 1, select);
				liPlaylist.setSelectedIndex(select + 1);
			}
		}
		else if(e.getSource()== btLike){
			if(liPlaylist.getSelectedValue() != null) {
				System.out.println("Like Button pressed");
				try {
					playerInstance.voteSong(liPlaylist.getSelectedIndex(), InetAddress.getLocalHost());
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			
		}
	}
	/**
	 * Sets the current played-file for highlighting it in the list.
	 * @param i
	 */
	public void setCurrent(final int i) {
		class PlaylistHighlighter extends DefaultListCellRenderer {   

		    private static final long serialVersionUID = 1L;
		    private int highlight;
		    public PlaylistHighlighter(int highlight) {
		    	this.highlight = highlight;
		    }
		    @SuppressWarnings("rawtypes")
		    public Component getListCellRendererComponent( JList list, Object value, int index, boolean isSelected, boolean cellHasFocus ) {   
		        Component c = super.getListCellRendererComponent( list, value, index, isSelected, cellHasFocus );   
		        if(index == highlight) {
		        	c.setBackground(Color.RED);
		        }
		        return c;   
		    } 
		}
		liPlaylist.setCellRenderer(new PlaylistHighlighter(i));
	}
	
	public void addUser(String user) {
		lmUser.addElement(user);
	}
	public void removeUser(String user) {
		lmUser.removeElement(user);
	}
	/**
	 * Sets the current amplitude in the ImageView.
	 * @param value
	 */
	public void setAmplitude(final byte value) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if(value < biAmp.length) {
					if(imAmp != null)
						imAmp.setImage(biAmp[value]);
				}
			}
		});
	}
	/**
	 * Sets the ID-tags to the right element in the GUI.
	 * @param interpret The artist of the music.
	 * @param album The single or album where the music was published.
	 * @param title The title of the file.
	 */
	public void setIDTags(String interpret, String album, String title) {
		lInterpret.setText(String.format("<html><div style=\"width:%dpx;\">%s</div><html>", 250, interpret));
		lTitle.setText(String.format("<html><div style=\"width:%dpx;\">%s</div><html>", 250, title));
		lAlbum.setText(String.format("<html><div style=\"width:%dpx;\">%s</div><html>", 250, album));
	}
	/**
	 * Adds an mp3 file to the list.<br/>
	 * It also checks if the given file has the right ending.
	 * @param file The file-object with the given mp3-file.
	 */
	public void addMP3Files(File file) {
		if(file.exists()) {
			if(file.getAbsolutePath().endsWith(".mp3") || 
					file.getAbsolutePath().endsWith(".MP3")) {
				playerInstance.addFile(file.getAbsolutePath());
			} else if(file.getAbsolutePath().endsWith(".m3u")
					|| file.getAbsolutePath().endsWith(".M3U")) {
				BufferedReader br = null;
				try {
					br = new BufferedReader(new FileReader(file));
					String line = null;
					while((line = br.readLine()) != null) {
						if(line.startsWith("/") || line.startsWith("C:\\")) {
							addMP3Files(new File(line));
						} else {
							addMP3Files(new File(file.getParentFile().getPath(), line));
						}
					}
					br.close();
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						if(br != null) {
							br.close();
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}
}
