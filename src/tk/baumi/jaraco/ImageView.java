package tk.baumi.jaraco;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * The image-view shows a specified image on a user-interface.<br/>
 * The class uses the JPanel from the swing-library.
 * @author Manuel Baumgartner
 *
 */
public class ImageView extends JPanel {

	private static final long serialVersionUID = -4820795519743685903L;
	private BufferedImage im;
	/**
	 * Location north means the picture will be on the top of the panel.
	 */
	private static final byte LOCATION_NORTH = 1;
	/**
	 * Location center means the picture will be in the center of the panel.
	 */
	private static final byte LOCATION_CENTER = 2;
	/**
	 * Location south means the picture will be on the bottom of the panel.
	 */
	private static final byte LOCATION_SOUTH = 3;
	/**
	 * The default location is showing the picture on top.
	 */
	private byte location = 1;
	private Dimension size;
	/**
	 * The background-color of the panel is set to blue.
	 */
	private Color cBack = Color.blue;
	
	public ImageView() {
		
	}
	/**
	 * The constructor needs the particular image and the location.
	 * @param im The picture as Buffered-image
	 * @param location The location as byte
	 * 		<ul>
	 * 		<li>1 for NORTH</li>
	 * 		<li>2 for CENTER</li>
	 * 		<li>3 for SOUTH</li>
	 * 		</ul>
	 * 		It is also possible to use on of the constants from ImageView.
	 */
	public ImageView(BufferedImage im, byte location) {
		this.location = location;
		setImage(im);
	}
	/**
	 * Initializes an ImageView with the preferred-size of the given image-dimensions.
	 * @param im The tmage as BufferdImage
	 * @param bSize true if the dimensions should be adapted.
	 */
	public ImageView(BufferedImage im, boolean bSize) {
		this.im = im;
		if(bSize) { 
			size = new Dimension(im.getWidth(),im.getHeight());
		}
		setPreferredSize(size);
	}
	public void setSize(Dimension dim) {
		size = dim;
	}
	
	public void setSize() {
		size = new Dimension(im.getWidth(),im.getHeight());
		setPreferredSize(size);
	}
	public void setLocation(byte location) {
		this.location = location;
	}
	
	public void setBackground(Color color) {
		cBack = color;
	}
	
	public void setImage(BufferedImage im) {
		this.im = im;
		repaint();
	}
	
	public BufferedImage getImage() {
		return im;
	}
	
	public void update(Graphics g) {
		paintComponent(g);
	}
	@Override
	public void paintComponent(Graphics g) {
		if(im != null) {
			g.setColor(cBack);
			g.fillRect(0, 0, getWidth(), getHeight());
			switch(location) {
			case LOCATION_NORTH:
				g.drawImage(im, 0,0,null);
				break;
			case LOCATION_CENTER:
				g.drawImage(im, 0, getSize().height / 2 - im.getHeight() / 2, null);
				break;
			case LOCATION_SOUTH:
				g.drawImage(im, 0, getSize().height - im.getHeight(), null);
				break;
			}
		}
	}

}
