# README #

Install Jaraco:

You need a Java-Runtime-Environment with the version 1.6 minimum.
The JRE can be downloaded at [Oracle.com](http://java.com/en/download/index.jsp)

When the JRE is installed you just have to open the Jaraco.jar file with the JRE.
Typically jar-files uses the Java automatically when you want to open it.